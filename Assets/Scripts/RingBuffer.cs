﻿using System;

public class RingBuffer<T>
{
    private readonly T[] _array;
    private readonly int _size;
    
    private int _head;
    private int _length;
    

    public RingBuffer(int size)
    {
        _array = new T[size];
        _head = 0;
        _length = 0;
        _size = size;
    }

    public int Length()
    {
        return _length;
    }

    public void Add(T value)
    {
        _head = (_head + 1) % _size;
        _array[_head] = value;
        if (_length < _size) _length++;
    }

    public T Get(int idx)
    {
        if (idx >= _length)
        {
            throw new ArgumentOutOfRangeException();
        }

        return _array[(_head - idx + _size) % _size];
    }
    
    
}