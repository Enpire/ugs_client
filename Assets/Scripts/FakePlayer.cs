﻿using System.Collections;
using Enpire.UGS.ClientProtocol.Messages;
using Enpire.UGS.Protocol.Messages;
using Google.Protobuf;
using UnityEngine;

public class FakePlayer : MonoBehaviour
{

    public RoomLink link;
    public float radius;
    public float speed;

    void OnEnable()
    {
        link.OnSendState += SendUpdate;
    }

    void OnDisable()
    {
        link.OnSendState -= SendUpdate;
    }


    void Update()
    {
        transform.position = Quaternion.Euler(0, 0, Time.time * speed) * Vector3.right * radius;
    }

    void SendUpdate()
    {
        print("Fake player sends update");
        var state = new StateUpdate
        {
            Id = 1,
            Hp = 0,
            Rotation = 0, 
            State = PlayerState.Playing,
            X = transform.position.x,
            Y = transform.position.y,
        };
        var clientMessage = new ClientProtocolMessage
        {
            StateUpdate = state
        };
        link.SendMessage(new GameRoomMessage
        {
            BroadcastMessage = new BroadcastMessage
            {
                Content = clientMessage.ToByteString()
            }
        });
    }

}