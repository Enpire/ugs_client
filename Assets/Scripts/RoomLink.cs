﻿using System;
using System.Collections;
using Enpire.UGS.Protocol.Messages;
using Google.Protobuf;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Enpire.UGS.ClientProtocol.Messages;

public class RoomLink : MonoBehaviour
{
    public delegate void OnSendStateEvent();

    public string host;
    public int port;

    public float sendRate;
    
    public int playerId;
    public Dictionary<string, ByteString> roomVariables = new Dictionary<string, ByteString>();
    public List<Player> players = new List<Player>();
    public Player playerPrefab;

    public OnSendStateEvent OnSendState;
    
    private float lastMessageTime;

    private TCPSocketConnection connection;

    void OnDisable()
    {
        connection?.Stop();
        connection = null;
    }

    private void OnApplicationQuit()
    {
        connection?.Stop();
        connection = null;
    }

    [ContextMenu("Connect")]
    public void Connect (string host, int port)
    {
        this.host = host;
        this.port = port;
        connection = new TCPSocketConnection(host, port, this, OnDataReceived);
    }

    IEnumerator SendStates()
    {
        while (true)
        {
            yield return new WaitForSeconds(1F / sendRate);
            SendLocalItemStates();
        }
    }

    void OnDataReceived(byte[] data)
    {
        var message = GameRoomMessage.Parser.ParseFrom(data);
        lastMessageTime = Time.time;
        
        switch (message.MessageContentCase)
        {
            case GameRoomMessage.MessageContentOneofCase.Command:
                switch (message.Command)
                {
                    case RoomCommand.SyncRequest:
                        ReplyLocalItemStates(message.UserId);
                        break;
                }
                break;
            case GameRoomMessage.MessageContentOneofCase.UserEntered:
                playerId = message.UserEntered.Id;
                CreateLocalItems();
                StartCoroutine(SendStates());
                SendSyncRequest();
                break;
            case GameRoomMessage.MessageContentOneofCase.DirectMessage:
                ProcessClientProtocolMessage(message.DirectMessage.Content);
                break;
            case GameRoomMessage.MessageContentOneofCase.BroadcastMessage:
                ProcessClientProtocolMessage(message.BroadcastMessage.Content);
                break;
            case GameRoomMessage.MessageContentOneofCase.RoomVariables:
                foreach (var roomVariable in message.RoomVariables.Variables)
                {
                    roomVariables[roomVariable.Key] = roomVariable.Value;
                }
                break;
        }
    }

    void SendSyncRequest()
    {
        SendMessage(new GameRoomMessage
        {
            Command = RoomCommand.SyncRequest,
        });
    }

    void ProcessClientProtocolMessage(ByteString directMessageContent)
    {
        var message = ClientProtocolMessage.Parser.ParseFrom(directMessageContent);
        switch (message.ContentCase)
        {
            case ClientProtocolMessage.ContentOneofCase.StateUpdate:
                var state = message.StateUpdate;
                var player = players.Find(p => p.playerId == state.Id);
                if (player == null && playerId != state.Id)
                {
                    player = Instantiate(playerPrefab);
                    player.playerId = state.Id;
                    player.isLocal = state.Id == playerId;
                    player.Setup(sendRate);
                    players.Add(player);
                }

                if (player != null) player.SetState(state);
                break;
        }
    }

    void CreateLocalItems()
    {
        var player = Instantiate(playerPrefab);
        player.playerId = playerId;
        player.isLocal = true;
        players.Add(player);
    }

    void SendLocalItemStates()
    {
        foreach (var player in players.Where(p => p.isLocal && p.IsDirty))
        {
            player.IsDirty = false;
            var message = new ClientProtocolMessage
            {
                StateUpdate = player.GetState()
            };
            SendMessage(new GameRoomMessage
            {
                BroadcastMessage = new BroadcastMessage
                {
                    Content = message.ToByteString()
                }
            });
        }
        
        OnSendState?.Invoke();
    }

    void ReplyLocalItemStates(int requestingId)
    {
        foreach (var player in players.Where(p => p.isLocal))
        {
            var message = new ClientProtocolMessage
            {
                StateUpdate = player.GetState()
            };
            SendMessage(new GameRoomMessage
            {
                DirectMessage = new DirectMessage
                {
                    UserId = requestingId,
                    Content = message.ToByteString()
                }
            });
        }
    }

    public void SendMessage(GameRoomMessage msg)
    {
        connection.Send(msg.ToByteArray());
    }
}