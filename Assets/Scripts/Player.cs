﻿using System;
using Enpire.UGS.ClientProtocol.Messages;
using UnityEngine;

namespace Enpire.UGS.Protocol.Messages
{
    public class Player : MonoBehaviour
    {
        public NetworkedTransform t;

        public int playerId;
        public bool isLocal;
        public float speed;

        void Update()
        {
            if (isLocal)
            {
                var axisH = Input.GetAxis("Horizontal");
                var axisV = Input.GetAxis("Vertical");
                if (Mathf.Abs(axisH) > Mathf.Epsilon || Mathf.Abs(axisV) > Mathf.Epsilon)
                {
                    var pos = t.GetPosition();
                    pos.x += axisH * Time.deltaTime * speed;
                    pos.y += axisV * Time.deltaTime * speed;
                    t.SetPosition(pos);
                    t.IsDirty = true;
                }
                if (Input.GetKey(KeyCode.Mouse0))
                {
                    var pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    pos.z = 0;
                    t.SetPosition(pos);
                    t.IsDirty = true;
                }
            }
        }

        public void Setup(float sendRate)
        {

            if (!isLocal)
            {
                t.updateRate = sendRate;
                t.interpolate = true;
            }
        }

        public bool IsDirty
        {
            get { return t.IsDirty; }
            set { t.IsDirty = value; }
        }

        public StateUpdate GetState()
        {
            var pos = t.GetPosition();
            return new StateUpdate
            {
                Id = playerId,
                Hp = 0,
                Rotation = t.GetRotation(), 
                State = PlayerState.Playing,
                X = pos.x,
                Y = pos.y,
            };
        }

        public void SetState(StateUpdate state)
        {
            if (!isLocal)
            {
                t.SetPosition(new Vector2(state.X, state.Y));
                t.SetRotation(state.Rotation);
            }
        }
    }
}