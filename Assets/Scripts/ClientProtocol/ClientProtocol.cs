// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: client_protocol.proto
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace Enpire.UGS.ClientProtocol.Messages {

  /// <summary>Holder for reflection information generated from client_protocol.proto</summary>
  public static partial class ClientProtocolReflection {

    #region Descriptor
    /// <summary>File descriptor for client_protocol.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static ClientProtocolReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "ChVjbGllbnRfcHJvdG9jb2wucHJvdG8iSAoVQ2xpZW50UHJvdG9jb2xNZXNz",
            "YWdlEiQKDHN0YXRlX3VwZGF0ZRgBIAEoCzIMLlN0YXRlVXBkYXRlSABCCQoH",
            "Q29udGVudCJqCgtTdGF0ZVVwZGF0ZRIKCgJpZBgBIAEoBRIbCgVzdGF0ZRgC",
            "IAEoDjIMLlBsYXllclN0YXRlEgkKAXgYAyABKAISCQoBeRgEIAEoAhIQCghy",
            "b3RhdGlvbhgFIAEoAhIKCgJocBgGIAEoBSokCgtQbGF5ZXJTdGF0ZRIICgRE",
            "ZWFkEAASCwoHUGxheWluZxABQiWqAiJFbnBpcmUuVUdTLkNsaWVudFByb3Rv",
            "Y29sLk1lc3NhZ2VzYgZwcm90bzM="));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { },
          new pbr::GeneratedClrTypeInfo(new[] {typeof(global::Enpire.UGS.ClientProtocol.Messages.PlayerState), }, new pbr::GeneratedClrTypeInfo[] {
            new pbr::GeneratedClrTypeInfo(typeof(global::Enpire.UGS.ClientProtocol.Messages.ClientProtocolMessage), global::Enpire.UGS.ClientProtocol.Messages.ClientProtocolMessage.Parser, new[]{ "StateUpdate" }, new[]{ "Content" }, null, null),
            new pbr::GeneratedClrTypeInfo(typeof(global::Enpire.UGS.ClientProtocol.Messages.StateUpdate), global::Enpire.UGS.ClientProtocol.Messages.StateUpdate.Parser, new[]{ "Id", "State", "X", "Y", "Rotation", "Hp" }, null, null, null)
          }));
    }
    #endregion

  }
  #region Enums
  public enum PlayerState {
    [pbr::OriginalName("Dead")] Dead = 0,
    [pbr::OriginalName("Playing")] Playing = 1,
  }

  #endregion

  #region Messages
  public sealed partial class ClientProtocolMessage : pb::IMessage<ClientProtocolMessage> {
    private static readonly pb::MessageParser<ClientProtocolMessage> _parser = new pb::MessageParser<ClientProtocolMessage>(() => new ClientProtocolMessage());
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<ClientProtocolMessage> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Enpire.UGS.ClientProtocol.Messages.ClientProtocolReflection.Descriptor.MessageTypes[0]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public ClientProtocolMessage() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public ClientProtocolMessage(ClientProtocolMessage other) : this() {
      switch (other.ContentCase) {
        case ContentOneofCase.StateUpdate:
          StateUpdate = other.StateUpdate.Clone();
          break;
      }

    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public ClientProtocolMessage Clone() {
      return new ClientProtocolMessage(this);
    }

    /// <summary>Field number for the "state_update" field.</summary>
    public const int StateUpdateFieldNumber = 1;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Enpire.UGS.ClientProtocol.Messages.StateUpdate StateUpdate {
      get { return contentCase_ == ContentOneofCase.StateUpdate ? (global::Enpire.UGS.ClientProtocol.Messages.StateUpdate) content_ : null; }
      set {
        content_ = value;
        contentCase_ = value == null ? ContentOneofCase.None : ContentOneofCase.StateUpdate;
      }
    }

    private object content_;
    /// <summary>Enum of possible cases for the "Content" oneof.</summary>
    public enum ContentOneofCase {
      None = 0,
      StateUpdate = 1,
    }
    private ContentOneofCase contentCase_ = ContentOneofCase.None;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public ContentOneofCase ContentCase {
      get { return contentCase_; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void ClearContent() {
      contentCase_ = ContentOneofCase.None;
      content_ = null;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as ClientProtocolMessage);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(ClientProtocolMessage other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (!object.Equals(StateUpdate, other.StateUpdate)) return false;
      if (ContentCase != other.ContentCase) return false;
      return true;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (contentCase_ == ContentOneofCase.StateUpdate) hash ^= StateUpdate.GetHashCode();
      hash ^= (int) contentCase_;
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (contentCase_ == ContentOneofCase.StateUpdate) {
        output.WriteRawTag(10);
        output.WriteMessage(StateUpdate);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (contentCase_ == ContentOneofCase.StateUpdate) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(StateUpdate);
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(ClientProtocolMessage other) {
      if (other == null) {
        return;
      }
      switch (other.ContentCase) {
        case ContentOneofCase.StateUpdate:
          StateUpdate = other.StateUpdate;
          break;
      }

    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            input.SkipLastField();
            break;
          case 10: {
            global::Enpire.UGS.ClientProtocol.Messages.StateUpdate subBuilder = new global::Enpire.UGS.ClientProtocol.Messages.StateUpdate();
            if (contentCase_ == ContentOneofCase.StateUpdate) {
              subBuilder.MergeFrom(StateUpdate);
            }
            input.ReadMessage(subBuilder);
            StateUpdate = subBuilder;
            break;
          }
        }
      }
    }

  }

  public sealed partial class StateUpdate : pb::IMessage<StateUpdate> {
    private static readonly pb::MessageParser<StateUpdate> _parser = new pb::MessageParser<StateUpdate>(() => new StateUpdate());
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<StateUpdate> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Enpire.UGS.ClientProtocol.Messages.ClientProtocolReflection.Descriptor.MessageTypes[1]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public StateUpdate() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public StateUpdate(StateUpdate other) : this() {
      id_ = other.id_;
      state_ = other.state_;
      x_ = other.x_;
      y_ = other.y_;
      rotation_ = other.rotation_;
      hp_ = other.hp_;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public StateUpdate Clone() {
      return new StateUpdate(this);
    }

    /// <summary>Field number for the "id" field.</summary>
    public const int IdFieldNumber = 1;
    private int id_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int Id {
      get { return id_; }
      set {
        id_ = value;
      }
    }

    /// <summary>Field number for the "state" field.</summary>
    public const int StateFieldNumber = 2;
    private global::Enpire.UGS.ClientProtocol.Messages.PlayerState state_ = 0;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Enpire.UGS.ClientProtocol.Messages.PlayerState State {
      get { return state_; }
      set {
        state_ = value;
      }
    }

    /// <summary>Field number for the "x" field.</summary>
    public const int XFieldNumber = 3;
    private float x_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public float X {
      get { return x_; }
      set {
        x_ = value;
      }
    }

    /// <summary>Field number for the "y" field.</summary>
    public const int YFieldNumber = 4;
    private float y_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public float Y {
      get { return y_; }
      set {
        y_ = value;
      }
    }

    /// <summary>Field number for the "rotation" field.</summary>
    public const int RotationFieldNumber = 5;
    private float rotation_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public float Rotation {
      get { return rotation_; }
      set {
        rotation_ = value;
      }
    }

    /// <summary>Field number for the "hp" field.</summary>
    public const int HpFieldNumber = 6;
    private int hp_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int Hp {
      get { return hp_; }
      set {
        hp_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as StateUpdate);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(StateUpdate other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (Id != other.Id) return false;
      if (State != other.State) return false;
      if (X != other.X) return false;
      if (Y != other.Y) return false;
      if (Rotation != other.Rotation) return false;
      if (Hp != other.Hp) return false;
      return true;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (Id != 0) hash ^= Id.GetHashCode();
      if (State != 0) hash ^= State.GetHashCode();
      if (X != 0F) hash ^= X.GetHashCode();
      if (Y != 0F) hash ^= Y.GetHashCode();
      if (Rotation != 0F) hash ^= Rotation.GetHashCode();
      if (Hp != 0) hash ^= Hp.GetHashCode();
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (Id != 0) {
        output.WriteRawTag(8);
        output.WriteInt32(Id);
      }
      if (State != 0) {
        output.WriteRawTag(16);
        output.WriteEnum((int) State);
      }
      if (X != 0F) {
        output.WriteRawTag(29);
        output.WriteFloat(X);
      }
      if (Y != 0F) {
        output.WriteRawTag(37);
        output.WriteFloat(Y);
      }
      if (Rotation != 0F) {
        output.WriteRawTag(45);
        output.WriteFloat(Rotation);
      }
      if (Hp != 0) {
        output.WriteRawTag(48);
        output.WriteInt32(Hp);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (Id != 0) {
        size += 1 + pb::CodedOutputStream.ComputeInt32Size(Id);
      }
      if (State != 0) {
        size += 1 + pb::CodedOutputStream.ComputeEnumSize((int) State);
      }
      if (X != 0F) {
        size += 1 + 4;
      }
      if (Y != 0F) {
        size += 1 + 4;
      }
      if (Rotation != 0F) {
        size += 1 + 4;
      }
      if (Hp != 0) {
        size += 1 + pb::CodedOutputStream.ComputeInt32Size(Hp);
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(StateUpdate other) {
      if (other == null) {
        return;
      }
      if (other.Id != 0) {
        Id = other.Id;
      }
      if (other.State != 0) {
        State = other.State;
      }
      if (other.X != 0F) {
        X = other.X;
      }
      if (other.Y != 0F) {
        Y = other.Y;
      }
      if (other.Rotation != 0F) {
        Rotation = other.Rotation;
      }
      if (other.Hp != 0) {
        Hp = other.Hp;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            input.SkipLastField();
            break;
          case 8: {
            Id = input.ReadInt32();
            break;
          }
          case 16: {
            state_ = (global::Enpire.UGS.ClientProtocol.Messages.PlayerState) input.ReadEnum();
            break;
          }
          case 29: {
            X = input.ReadFloat();
            break;
          }
          case 37: {
            Y = input.ReadFloat();
            break;
          }
          case 45: {
            Rotation = input.ReadFloat();
            break;
          }
          case 48: {
            Hp = input.ReadInt32();
            break;
          }
        }
      }
    }

  }

  #endregion

}

#endregion Designer generated code
