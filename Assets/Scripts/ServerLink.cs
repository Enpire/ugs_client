﻿using System.Collections;
using Enpire.UGS.Protocol.Messages;
using Google.Protobuf;
using UnityEngine;

public class ServerLink : MonoBehaviour
{

	public string host;
	public int port;

	public RoomLink roomLink;

	private TCPSocketConnection connection;

	[ContextMenu("Connect")]
	void Start () {
		connection = new TCPSocketConnection(host, port, this, OnDataReceived);
		StartCoroutine(AutoConnect());
	}
	
	void OnDisable()
	{
		connection?.Stop();
		connection = null;
	}

	private void OnApplicationQuit()
	{
		connection?.Stop();
		connection = null;
	}

	IEnumerator AutoConnect()
	{
		yield return new WaitForSeconds(1);
		RequestGameRoom();
	}

	[ContextMenu("SendTestMessage")]
	public void RequestGameRoom()
	{
		var message = new Packet
		{
			RequestGameRoom = new RequestGameRoom
			{
				Props = new GameRoomProps
				{
					BundleId = Application.identifier,
					Version = Application.version,
					Options = ""
				},
				Conditions = new GameRoomConditions()
			}
		};
		connection.Send(message.ToByteArray());
	}

	void OnDataReceived(byte[] data)
	{
		var packet = Packet.Parser.ParseFrom(data);
		switch (packet.PacketContentCase)
		{
			case Packet.PacketContentOneofCase.GameRoomList:
				if (packet.GameRoomList.Rooms.Count > 0)
				{
					var roomData = packet.GameRoomList.Rooms[0];
					roomLink.Connect(roomData.Address, roomData.Port);
				}
				break;
			case Packet.PacketContentOneofCase.DebugMessage:
				print("Debug message: " + packet.DebugMessage);
				break;
		}
	}
}
