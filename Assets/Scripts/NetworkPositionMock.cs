﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class NetworkPositionMock : MonoBehaviour
{
	[Serializable]
	public class EventVector3 : UnityEvent<Vector3> {}

	public float sendRate = 50;
	public float delay = 0.1F;

	public EventVector3 Setter;

	// Use this for initialization
	void Start ()
	{
		StartCoroutine(SetFlow());
	}

	IEnumerator SetFlow()
	{
		while (true)
		{
			
			yield return new WaitForSeconds(1F / sendRate);
			if (delay <= Mathf.Epsilon)
			{
				Setter.Invoke(transform.position);
			}
			else
			{
				StartCoroutine(SetWithDelay(transform.position));
			}
		}
	}

	IEnumerator SetWithDelay(Vector3 value)
	{
		yield return new WaitForSeconds(delay);
		Setter.Invoke(value);
	}
	
}
