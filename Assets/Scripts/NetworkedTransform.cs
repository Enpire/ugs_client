﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Enpire.UGS.Protocol.Messages
{
    public class NetworkedTransform : MonoBehaviour
    {

        public float updateRate;
        public float delay;
        public bool interpolate;
        public bool predict;

        public float progressDump = 0.95F;
        public float progressClamp = 2F;
        public float transformLerpDump = 15F;
        public float jitterDump = 0.75F;
        public float jitterClamp = 2F;

        public bool IsDirty;
        private float lastUpdateTime;

        public RingBuffer<Vector2> positions = new RingBuffer<Vector2>(2);

        void LateUpdate()
        {
            if (positions.Length() < 2 || !interpolate && !predict) return;
            
            var to = predict 
                ? PredictedPosition()
                : positions.Get(0);
            var from = interpolate 
                ? predict 
                    ? positions.Get(0) 
                    : positions.Get(1)
                : to;
            var progress = Mathf.Clamp((Time.time - lastUpdateTime) * updateRate, 0F, progressClamp) * progressDump;
            var desiredPosition = Vector2.LerpUnclamped(
                from,
                to,
                progress
            );
            transform.position = Vector2.Lerp(
                transform.position, 
                desiredPosition, 
                transformLerpDump * Time.deltaTime 
            );
        }

        Vector2 PredictedPosition()
        {
            var from = positions.Get(1);
            var to  = positions.Get(0);
            var progress = 1F + delay * updateRate + 1F / updateRate;
            return Vector2.LerpUnclamped(from, to, progress);
        }

        public void SetPosition(Vector2 position)
        {
            var pos = transform.position;
            pos.x = position.x;
            pos.y = position.y;
            positions.Add(pos);
            var frameDuration = 1F / updateRate;
            var jitter = Time.time - lastUpdateTime - frameDuration;
            lastUpdateTime = Time.time - jitter * jitterDump;

            if (positions.Length() < 2 || (!interpolate && !predict))
            {
                transform.position = pos;
            }
        }

        public void SetPosition(Vector3 position)
        {
            SetPosition((Vector2)position);
        }

        public Vector2 GetPosition()
        {
            return transform.position;
        }

        public void SetRotation(float z)
        {
            var rotation = transform.rotation;
            rotation.z = z;
            transform.rotation = rotation;
        }

        public float GetRotation()
        {
            return transform.rotation.z;
        }
    }
}