﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Collections.Concurrent;
using System.Text;
using System.Threading;
using UnityEngine;

public class TCPSocketConnection {

	public TCPSocketConnection(string host, int port, MonoBehaviour ctx, Action<byte[]> callback)
	{
		this.host = host;
		this.port = port;
		ConnectToTcpServer();
		ctx.StartCoroutine(ReceiveData(callback)); 
		ctx.StartCoroutine(SendData());
	}

	~TCPSocketConnection()
	{
		Stop();	
	}
	
	#region private members 	
	private string host;
	private int port;
	private TcpClient socketConnection; 	
	private Thread clientReceiveThread;
	private ConcurrentQueue<byte[]> incommingMessages = new ConcurrentQueue<byte[]>();
	private ConcurrentQueue<byte[]> outgoingMessages = new ConcurrentQueue<byte[]>();
	private bool stopped = false;
	#endregion

	public void Send(byte[] msg)
	{
		outgoingMessages.Enqueue(msg);
	}

	public void Stop()
	{
		stopped = true;
		outgoingMessages = new ConcurrentQueue<byte[]>();
		incommingMessages = new ConcurrentQueue<byte[]>();
		socketConnection?.Close();
	}

	
	private void ConnectToTcpServer () { 		
		try {  			
			clientReceiveThread = new Thread (ListenForData); 			
			clientReceiveThread.IsBackground = true; 			
			clientReceiveThread.Start();  		
		} 		
		catch (Exception e) { 			
			Debug.Log("On client connect exception " + e); 		
		} 	
	}  	
	
	private void ListenForData() { 		
		try { 			
			socketConnection = new TcpClient(host, port);
			socketConnection.NoDelay = true;
			Byte[] length = new Byte[2];             
			while (!stopped) { 		
				var stream = socketConnection.GetStream();
				stream.Read(length, 0, length.Length);
				var data = new byte[length[0] * 256 + length[1]];
				if (stream.Read(data, 0, data.Length) > 0)
				{
					incommingMessages.Enqueue(data);
				}
			}   
			Debug.Log("Stopping");
		}         
		catch (SocketException socketException) {             
			Debug.Log("Socket exception: " + socketException);         
		}     
	}  	

	IEnumerator ReceiveData(Action<byte[]> callback)
	{
		while (!stopped)
		{
			while (incommingMessages.Count > 0)
			{
				byte[] msg;
				if (incommingMessages.TryDequeue(out msg))
				{
					callback.Invoke(msg);
				}
			}

			yield return null;
		}
	}

	IEnumerator SendData()
	{
		while (!stopped)
		{
			while (outgoingMessages.Count > 0)
			{
				byte[] message;
				if (!outgoingMessages.TryDequeue(out message)) continue;
				
				var size = new byte[2];
				size[0] = (byte) (message.Length / 256);
				size[1] = (byte) (message.Length % 256);
				try { 						
					var stream = socketConnection.GetStream(); 			
					if (stream.CanWrite) {
						stream.Write(size, 0, size.Length);
						stream.Write(message, 0, message.Length);                            
					}         
				} 		
				catch (SocketException socketException) {             
					Debug.Log("Socket exception: " + socketException);       
					// TODO consider to return message to queue
				}     
			}

			yield return null;
		}
	}
}